class Board
  DISPLAY_HASH = {
  nil => " ",
  :s => " ",
  :x => "x"
}
  attr_accessor :grid
  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, val = nil)
    row, col = pos
    @grid[row][col] = val
  end

  def won?
    @grid.flatten.none? { |el| el == :s }
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end
  def self.random
    self.new(self.default_grid, true)
  end
  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "error" if full?
    pos = random_pos
      until empty?(pos)
        pos = random_pos
      end
    self[pos] = :s
  end

  def random_pos
    [rand(size), rand(size)]
  end
  def randomize(count = 10)
    count.times { place_random_ship }
  end

  def count
    grid.flatten.select { |el| el == :s }.length
  end

  def size
    @grid.length
  end

  def display
    header = (0..9).to_a.join("  ")
    p "  #{header}"
    grid.each_with_index { |row, i| display_row(row, i) }
  end

  def display_row(row, i)
    chars = row.map { |el| DISPLAY_HASH[el] }.join("  ")
    p "#{i} #{chars}"
  end
  def in_range?(pos)
    pos.all? { |x| x.between?(0, grid.length - 1) }
  end

  def empty?(pos = [0, 0])
    row, col = pos
    if @grid[row][col] == nil
      return true
    else
      return false
    end
  end

end
