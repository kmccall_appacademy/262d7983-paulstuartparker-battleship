require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board )
    @board = board
    @player = player
    @hit = false
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end
  #
  def [](pos)
    row, col = pos
    @board[row][col]
  end

  def []=(pos, val = nil)
    row, col = pos
    @board[row][col] = val
  end

  def count
    board.count
  end

  def hit?
    @hit
  end


  def attack(pos)
    row, col = pos
    if @board[pos] == :s
      @hit = true
    else
      @hit = false
    end
     @board[pos] = :x
  end

  def declare_winner
    puts "Congratulations. You win!"
  end

  def display_status
    system("clear")
    board.display
    puts "It's a hit!" if hit?
    puts "There are #{count} ships remaining."
  end

  def play
    play_turn until game_over?
    declare_winner
  end

  def play_turn
    pos = nil

    until valid_play?(pos)
      display_status
      pos = player.get_play
    end

    attack(pos)
  end

  def valid_play?(pos)
    pos.is_a?(Array) && board.in_range?(pos)
  end



end
if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
